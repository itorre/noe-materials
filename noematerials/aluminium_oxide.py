"""
Material parameters for aluminium oxide
"""

from .common import *

cm_1 = 2*np.pi*lightspeed*1e2   # angular frequency from cm^-1

def permittivity_Kischkat(omega):
    """
    Permittivity of sputtered AlO2. Valid for about 18-195 THz (700-6500 cm^-1)

    From Kischkat et al, doi:10.1364/AO.51.006789 .
    We have chosen the "AO20-200" dataset as it looks typical.
    """

#    Note - Paper is confusing!!!
#    \sigma is defined differently between table and equation. In the
#    table it is FWHM but in the equation it is standard deviation.
    fac = 1/(2*np.sqrt(2*np.log(2)))

    eps = 2.11 + 0.j*omega # baseline
    eps += ( 735*cm_1)**2 * lor_brendel(omega,  190*cm_1,  18  *cm_1,1286*cm_1*fac) # osc 1
    eps += ( 726*cm_1)**2 * lor_brendel(omega,  715*cm_1, 1e-50*cm_1, 254*cm_1*fac) # osc 2
    eps += (43193./221086.)**2 # third oscillator is essentially this flat contribution.

    return eps

permittivity = permittivity_Kischkat
