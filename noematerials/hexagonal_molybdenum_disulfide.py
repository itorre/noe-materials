"""
Material parameters for hexagonal MoS2
"""

from .common import *
import warnings

cm_1 = 2*np.pi*lightspeed*1e2   # angular frequency from cm^-1

def permittivity_Wieting(omega):
    """
    Permittivity of hexagonal MoS2, divided by permittivity of freespace.
    This is a two-component permittivity for in-plane electric field,
    out-of-plane electric field.

    This is based on Wieting & Verble, 1970: PhysRevB.3.4286

    Valid for mid-infrared frequencies, does not include things like
    free carrier (Drude) response for doped MoS2, nor interband transitions.
    """
    perp = (15.2
            + 0.2010619 * lor(omega, 384.*cm_1, 0.96*cm_1)
            )
    par  = ( 6.2
            + 0.0301593 * lor(omega, 470.*cm_1, 2.35*cm_1)
            )
    return perp, par

permittivity = permittivity_Wieting
