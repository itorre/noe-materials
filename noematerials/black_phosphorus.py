"""
Material parameters for black phosphorus.

Axis conventions
----------------
 - x axis is in-plane, perpendicular to the puckering ripples
 - y axis is in-plane, parallel to the puckering ripples
 - z axis is perpendicular to the planes

The x,y,z axes are also known as c,a,b respectively.

Resources
---------

A good resource for black P electronic and optical properties is Morita's
1986 review article "Semiconducting black phosphorus" ::

    doi:10.1007/BF00617267

For far-mid infrared properties, we have phonons at 4 and 14 THz, aligned
on the x and z axes respectively. Both are fairly weak. The y axis does
not have an infrared active phonon. Dielectric constants are anisotropic
and around 8 to 16 in value.
(see Sugai 10.1016/0038-1098(85)90213-3 and Nagahama doi:10.1143/JPSJ.54.2096)

Plasmons in thin layers are discussed somewhat in Tony Low's paper,
doi:10.1103/PhysRevLett.113.106802


Conventions:
    Always use SI units!
        m^-2 for carrier density
        J for energies
        Hz for frequencies
        rad/s for angular frequencies
        1/s for damping rates
        etc.

    Imaginary part of epsilon should never be negative (gain).
    Real part of conductivity should never be negative (gain).

    When you add a material's parameters, you must provide citations to the
    relevant literature.
    Name the function specific, for example if you find the permittivity of
    SiO2 in a paper of Johnson et al., don't just call it SiO2.permittivity,
    but rather SiO2.permittivity_Johnson. Then if you think this is the
    "best" permittivity function to use, provide an alias.
"""

from .common import *

cm_1 = 2*np.pi*lightspeed*1e2   # angular frequency in s^-1 from cm^-1

layer_thickness = 1.07e-9 # out of plane lattice const in nm,  see PhysRevLett.113.106802

def permittivity_FIR_Nagahama():
    """
    Return three-component permittivity of BP (see axis convention in module
    docstring), for frequencies of 4 to 10 THz, between the two phonons.

    from Nagahama paper
    "Optical Determination of Dielectric Constant in Black Phosphorus"
    doi:10.1143/JPSJ.54.2096

    The in-plane constants were measured using transmittance through thin
    samples with interference effects;
    """
    return (16.5, 13.0, 8.3)

def permittivity_FIR_Asahina():
    """
    Return three-component permittivity of BP (see axis convention in module
    docstring), for very far infrared (< 3 THz), below phonons.

    This is from a theoretical calculation of "self-consistent pseudopotential
    method" in "Optical reflectivity and band structure of black phosphorus":
    doi:10.1016/0378-4363(83)90546-6
    """
    return (12.5, 10.2, 8.3)

def permittivity_phonon_Mark(omega):
    """
    Return three-component permittivity of BP (see axis convention in module
    docstring), for mid infrared frequencies below 80 THz (below band gap),
    including phonons.

    This is primarily based on experiments of Nagahama and Sugai, with some
    guidance from theory.
    """
    # e_x: Static 12.5 (Morita) or Highfreq 16.5 (Nagahama) - big mismatch!
    #      Phonon @ 136 (Sugai) or 129 (Morita); strength 0.36 - 1.4 ???
    # e_y : 10.2 (Morita) or 13.0 (Nagahama), no phonon
    # e_z : Static 8.3 (Morita, Nagahama)
    #      Phonon @ 468 (Sugai) or 461 (Morita): strength 0.035-0.07
    # dampings made to match Sugai
    ex = 16.5 + 1.*lor(omega, 136*cm_1, 0.5e12)
    ey = 13.0 + 0.*omega
    ez = 8.25 + 0.05*lor(omega, 468*cm_1, 0.5e12)
    return (ex,ey,ez)

def conductivity_bulk_Low(omega, n):
    """
    Estimate of free carrier conductivity for bulk BP with a given bulk
    electron density (m^-3). This is a three-component conductivity for
    axes x,y,z (see module docstring for definition).

    You can calculate (1.j / (omega * electric_constant)) * cond and add
    this to the background permittivity (including phonons) to get a full
    permittivity.

    This uses effective masses from Tony Low's paper
    (10.1103/PhysRevLett.113.106802) which are in turn an average of
    experimental and theoretical values.

    Mobilities from the Morita review which quotes values at 200 K,
    extrapolated to 300 K assuming the provided T^-1.5 law (54%)

    Intrinsic carrier density of 6e18 is taken according to density of
    states effective mass (note: BP is single valley) and 300 K, with the
    gap of 335 meV (Morita).

    Parameters
    ----------

    omega :
        angular frequency

    n :
        Free electron density (in m^-3), relative to intrinsic BP.
        This is equal to # of conduction band electrons minus # of valence
        band holes. `n` is positive for n-doped and negative for p-doped.
    """
    n_i = 6e18
    cx = conductivity_Drude_semiconductor(
                        omega,
                        n, n_i,
                        0.08, 0.08,
                        0.125, 0.163,
                        )
    cy = conductivity_Drude_semiconductor(
                        omega,
                        n, n_i,
                        0.7, 1.0,
                        0.025, 0.065,
                        )
    cz = conductivity_Drude_semiconductor(
                        omega,
                        n, n_i,
                        0.2, 0.4,
                        0.022, 0.029,
                        )
    return cx, cy, cz


def conductivity_monolayer_Low(omega, n):
    """
    Estimate of free carrier conductivity for bulk BP with a given SHEET
    electron density n (in m^-2). This is a two-component conductivity for
    axes x,y ; conduction does not occur along z.

    You can calculate (1.j / (omega * electric_constant * thickness)) * cond
    and add this to the background permittivity (including phonons) to get
    a full permittivity for a monolayer, treating it as a bulk (1nm) layer.

    This uses effective masses from Tony Low's paper
    (10.1103/PhysRevLett.113.106802).

    Mobilities are same as from the bulk case (see conductivity_bulk_Low).

    Intrinsic carrier density of 0 is taken, since monolayer has a large
    bangap and the real intrinsic density will be tiny.

    Parameters
    ----------

    omega :
        angular frequency

    n :
        Free electron density (in m^-2), relative to intrinsic BP.
        This is equal to # of conduction band electrons minus # of valence
        band holes. `n` is positive for n-doped and negative for p-doped.
    """
    n_i = 0
    cx = conductivity_Drude_semiconductor(
                        omega,
                        n, n_i,
                        0.15, 0.15,
                        0.125, 0.163,
                        )
    cy = conductivity_Drude_semiconductor(
                        omega,
                        n, n_i,
                        0.7, 1.0,
                        0.025, 0.065,
                        )
    return cx, cy




def permittivity_bulk(omega, n):
    """
    Total permittivity (background + phonon + Drude) by combination of
    permittivity_phonon_Mark() and conductivity_bulk_Low()
    """
    ex,ey,ez = permittivity_phonon_Mark(omega)
    cx,cy,cz = conductivity_bulk_Low(omega, n)
    ex = ex + 1.j*(cx)/(omega*electric_constant)
    ey = ey + 1.j*(cy)/(omega*electric_constant)
    ez = ez + 1.j*(cz)/(omega*electric_constant)
    return ex, ey, ez

def permittivity_monolayer(omega, n):
    """
    Total permittivity (background + phonon + Drude) by combination of
    permittivity_phonon_Mark() and conductivity_monolayer_Low().

    THIS IS ONLY VALID IF YOU MAKE THICKNESS EQUAL TO layer_thickness FROM
    THIS MODULE.
    """
    ex,ey,ez = permittivity_phonon_Mark(omega)
    cx,cy = conductivity_monolayer_Low(omega, n)
    ex = ex + 1.j*(cx)/(omega*electric_constant*layer_thickness)
    ey = ey + 1.j*(cy)/(omega*electric_constant*layer_thickness)
    ez = ez
    return ex, ey, ez
