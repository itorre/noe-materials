"""
Material parameters for silicon
"""

from .common import *

def permittivity_Bart_cleanSi(omega, n = 0.):
    """
    A permittivity model for clean silicon in mid-infrared and lower frequencies.
    For ROOM TEMPERATURE only. Does not include interband absorption!

    This uses conductivity_Drude_semiconductor() with parameters from Bart
    Zeghbroek's online book, in particular these pages:
    http://ecee.colorado.edu/~bart/book/effmass.htm#condmass
    http://ecee.colorado.edu/~bart/book/book/append/append3.htm

    The mobility used here is for impurity-free silicon.

    Parameters
    ----------

    omega :
        angular frequency

    n :
        Free electron density (in m^-3), relative to intrinsic Si.
        This is equal to # of conduction band electrons minus # of valence
        band holes. `n` is positive for n-doped silicon and negative for
        p-doped silicon.
    """

    cond = conductivity_Drude_semiconductor(
                                      omega,
                                      n, 1.0e16,
                                      0.26, 0.386,
                                      0.1400, 0.0450)
    return 11.9 + (1.j / (omega * electric_constant)) * cond

def permittivity_highpdoped_Cleary(omega):
    """
    Heavily p-doped silicon permittivity from:

    Cleary et al, "IR permittivities for silicides and doped silicon"
    http://dx.doi.org/10.1364/JOSAB.27.000730
    """
    cond = conductivity_Drude(omega, 6.4e25, 0.386*mass_electron, 1e14)
    return 11.7 + (1.j / (omega * electric_constant)) * cond

def permittivity_highndoped_Cleary(omega):
    """
    Heavily n-doped silicon permittivity from:

    Cleary et al, "IR permittivities for silicides and doped silicon"
    http://dx.doi.org/10.1364/JOSAB.27.000730
    """
    cond = conductivity_Drude(omega, 2.86e24, 0.26*mass_electron, 5.6e13)
    return 11.7 + (1.j / (omega * electric_constant)) * cond

permittivity = permittivity_Bart_cleanSi
