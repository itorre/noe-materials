"""
Material parameters for hexagonal boron nitride
"""

from .common import *
import warnings

cm_1 = 2*np.pi*lightspeed*1e2   # angular frequency from cm^-1

def permittivity_Geick(omega):
    """
    Epsilon of hexagonal boron nitride/epsilon_0.
    This is a two-component permittivity for in-plane electric field,
    out-of-plane electric field.

    This is based on Geick et al., 1966.
    Note that this BN is likely a fairly dirty sample with misaligned
    crystallites. It should not be used for exfoliated monocrystals of
    h-BN.
    """

    ## FROM GEICK (1966)
    perp = (4.95
            +(1.23e5/ 767.**2)*lor(omega, 767.*cm_1,35.*cm_1) #should be inactive
            +(3.49e6/1367.**2)*lor(omega,1367.*cm_1,29.*cm_1)
            )
    par  = (4.10
            +(3.25e5/ 783.**2)*lor(omega, 783.*cm_1, 8.*cm_1)
            +(1.04e6/1510.**2)*lor(omega,1510.*cm_1,80.*cm_1) # should be inactive
            )
    return perp, par

def permittivity_Cai(omega):
    """
    Epsilon of hexagonal boron nitride/epsilon_0.
    This is a two-component permittivity for in-plane electric field,
    out-of-plane electric field.

    This is based on Cai et al., 10.1016/j.ssc.2006.10.040 .
    """
    perp = (4.87
            +1.83*lor(omega, 1372.*cm_1, 0.)
            )
    par  = (2.95
            + 0.61*lor(omega, 746.*cm_1, 0.)
            )

    return perp, par

def permittivity_Cai_variable(omega,widthperp = 52.4, widthpar = 15.3):
    """
    Epsilon of hexagonal boron nitride/epsilon_0.
    This is a two-component permittivity for in-plane electric field,
    out-of-plane electric field.

    Optional parameters widthperp, widthpar are decay rates
    (in cm_1 -- WARNING: NON-CONSISTENT UNITS) to add losses to the
    Cai model (see permittivity_Cai) which does not specify losses.

    The default losses are made up.
    """
    warnings.warn('permittivity_Cai_variable is deprecated - WILL BE REMOVED')
    perp = (4.87
            +1.83*lor(omega, 1372.*cm_1, widthperp*cm_1)
            )
    par  = (2.95
            + 0.61*lor(omega, 746.*cm_1, widthpar*cm_1)
            )

    return perp, par

def permittivity_Cai_lossy(omega,decay_inplane=7*cm_1,decay_outplane=2*cm_1):
    """
    Epsilon of hexagonal boron nitride/epsilon_0.
    This is a two-component permittivity for in-plane electric field,
    out-of-plane electric field.

    Optional parameters decay_inplane, decay_outplane are amplitude decay
    rates (in s^-1) to add losses to the Cai model (see permittivity_Cai)
    which does not specify losses.
    Their default values are taken from permittivity_Caldwell().
    """
    perp = (4.87
            +1.83*lor(omega, 1372.*cm_1, decay_inplane)
            )
    par  = (2.95
            + 0.61*lor(omega, 746.*cm_1, decay_outplane)
            )

    return perp, par

def permittivity_Caldwell(omega):
    """
    Epsilon of hexagonal boron nitride/epsilon_0.
    This is a two-component permittivity for in-plane electric field,
    out-of-plane electric field.

    This is a "best guess" by J. Caldwell, used to produce Figure 1b in his
    paper arXiv:1404.0494.
    """
    perp = (4.90
            + 2.001*lor(omega, 1360.*cm_1, 7*cm_1)
            )
    par  = (2.95
            + 0.5262*lor(omega, 760.*cm_1, 2*cm_1)
            )

    return perp, par

permittivity = permittivity_Cai
