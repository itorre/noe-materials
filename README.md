# noe-materials (version 1.0)

## Description

Provides optical properties for various materials used in device fabrication.

## Available materials

* Alluminium Oxide
* Black Phosphorous
* Gold
* Graphene
* Hexagonal Boron Nitride
* Hexagonal Molibdenium Disulfide
* Silicon
* Silicon Oxide
* Titanium Oxide
* Two-dimensional electron gas



