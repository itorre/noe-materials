"""
Material parameters for graphene

* AC conductivity functions from various models
    - Simple Drude model
    - Local RPA incl. thermal smearing
    - Nonlocal RPA
    - Nonlocal RPA + relaxation time
* Utilities
    - Band offset-electron density relationship incl. thermal smearing

All functions rely on the module variable v_F -- Fermi velocity, simply set
to 10^6 m/s by default.


Conventions:
    Always use SI units!
        m^-2 for carrier density
        J for energies
        Hz for frequencies
        rad/s for angular frequencies
        etc.

    Imaginary part of epsilon should never be negative (gain).
    Real part of conductivity should never be negative (gain).

    When you add a material's parameters, you must provide citations to the
    relevant literature.
    Name the function specific, for example if you find the permittivity of
    SiO2 in a paper of Johnson et al., don't just call it SiO2.permittivity,
    but rather SiO2.permittivity_Johnson. Then if you think this is the
    "best" permittivity function to use, provide an alias.
"""

from .common import *
import warnings


try:
    import fdint
except ImportError:
    warnings.warn(
        'Could not import fdint package (download it from https://pypi.python.org/pypi/fdint). Some functionality will not be available.'

        )


###############
# DRUDE MODEL #
###############

def conductivity_Drude(omega, n_s, tau,v_F=1e6):
    """
    Graphene sheet conductivity (DRUDE MODEL) in terms of

    omega
        angular frequency in rad/s
    n_s
        sheet carrier density in m^-2
    tau
        transport scattering time in s
    """
    return (conductance_quantum*2.*v_F
            *np.sqrt(np.pi*abs(n_s))
            ) / (1./tau - 1.j*omega)



###################################
# LOCAL CONDUCTIVITY BEYOND DRUDE #
###################################

# Below we exploit arctanh to compute an integral ... BUT for lossless graphene
# (tau = infinity) this will run along a branch cut.
# We need to make sure our assumption is ok (regarding "signed zero") and
# all that. Behaviour of arctanh changed in numpy 1.10 from ignoring signed
# zero to respecting signed zero!

if np.arctanh(10+0.j).imag < 0. and np.arctanh(np.conj(10+0.j)).imag < 0.:
    ARCTANH_TYPE = 'unsigned'
elif np.arctanh(10+0.j).imag > 0. and np.arctanh(np.conj(10+0.j)).imag < 0.:
    ARCTANH_TYPE = 'signed'
else:
    raise RuntimeError('cannot identify arctanh branch cut!')

def _h_int(f, w):
    """
    Evaluation of internal energy integral for thermally smeared conductivity.
    h_int(f,w) = int(  (h(x) - h(w) / (w^2 - x^2),  x = 0..infty),
    where h(x) = sinh(x) / (cosh(f) + cosh(x))
      Here f stands for (mu - E_D)/kT,
           w stands for (hbar*omega/2)/kT.

    Returns the integral and also value of h(w)

    Strategy:
      1. The variations in this integral happen on a unity scale,
         so the x resolution is fixed at 0.05
      2. Note that for x - |f| >> 1, and x - |w| >> 1,
         we can basically treat h(x) as constant 1,
         so we need only integrate up to a little beyond f and
         then use exact integration thereafter.
      3. Watch out for NaNs where x == w ! For now we will just
         rely on the fact that this is unlikely. In the future
         maybe we could perform a local expansion for x ~= w.
    """
    f = np.abs(f)
    w *= np.sign(np.real(w)) # assure positive real part for w
    dx = 0.05 # resolution for variable of integration
    gobeyond = 10. # how many units (kT's) to go past the crossover

    # Set up integration variable range
    fmax = np.max(f)
    wmax = np.max(w.real)
    boundary = max(fmax,wmax)+gobeyond
    if boundary/dx > 1e6:
        raise ValueError('Too-big integral (temperature too low?). Bailing.')


    # make integration variable and also broadcasting version.
    x = np.arange(0., boundary, dx)
    xb = x.view()
    xb.shape = (x.size,) + (1,)*max(np.ndim(f), np.ndim(w))

    # calculate integrand
    cf = np.cosh(f)
    hw = np.sinh(w)/(cf + np.cosh(w))
    hx = np.sinh(xb)/(cf + np.cosh(xb))
    integrand = (hx - hw)/(w*w - xb*xb)

    # calculate integral on nontrivial range - trapezoid rule
    integral1 = dx*(np.sum(integrand,axis=0)
                    - 0.5*integrand[0]
                    - 0.5*integrand[-1])

    # Extrapolate past range end to calculate high x contribution.
    #  Note integral of 1/(b^2 - x^2) is atanh(x/b)/b + const,
    #  but atanh has a branch cut on real line so we must be careful.
    xmax = x[-1]
    wc = w*(1+0.j) # make sure w is complex for this part
    if ARCTANH_TYPE == 'unsigned':
        infatanh = ((wc.imag < 0) - 0.5)*(np.pi*1j) # atanh(inf/w)
    elif ARCTANH_TYPE == 'signed':
        infatanh = ((wc.imag <= 0) - 0.5)*(np.pi*1j) # atanh(inf/w)
    integral2 = (1. - hw)*(infatanh - np.arctanh(xmax/wc))/wc

    return hw, integral1 + integral2

def conductivity_localRPA(omega, E_D, tau, kT=4.0729e-21,v_F=1e6):
    """
    Graphene sheet conductivity in RPA model with thermal smearing, but
    only in the local approximation (k = 0), in terms of:

    omega
        angular frequency in rad/s
    E_D
        Dirac point energy relative to Fermi level, in J
        Note this is not proportional to square root of carrier density,
        due to smearing -- see ``electron_density_from_band_offset`` to
        map between these.
    tau
        relaxation time in s
    kT : optional
        boltzmann constant*temperature in J
        Default corresponds to 295 K.

    Comes from the supplement of:
      "Graphene Plasmonics: A Platform for Strong Light-Matter Interaction"
    can be found at:
      http://authors.library.caltech.edu/25493/2/nl201771h_si_001.pdf
    """

    f = -E_D/kT
    w = 0.5*hbar*omega/kT

    hw, integral = _h_int(f,w)

    cond = conductance_quantum*(
              4 * np.log(2*np.cosh(0.5*f)) * kT/hbar
                    / (1./tau - 1.j*omega)
              + (np.pi/2.)*hw + 1.j*w*integral
              )
    return cond



################
# NONLOCAL RPA #
################

# Check positivity of real part of square root
assert(np.sqrt(-1-0.1j).real >= 0)
assert(np.sqrt(-1+0.1j).real >= 0)
# Make sure log has imaginary part in [-pi,pi] interval
assert(abs(np.log(-1-0.001j).imag) <= np.pi)
assert(abs(np.log(-1+0.001j).imag) <= np.pi)

def conductivity_RPA0(omega,k_x,n_s,v_F=1e6):
    """
    Graphene sheet conductivity in RPA model with 0 temperature, 0 scattering,
    in terms of:

    omega
        angular frequency in rad/s
    k_x
        in-plane wavevector in rad/m
    n_s
        sheet carrier density in m^-2

    Comes from the supplement of:
      "Graphene Plasmonics: A Platform for Strong Light-Matter Interaction"
    can be found at:
      http://authors.library.caltech.edu/25493/2/nl201771h_si_001.pdf

    Note that the original equation there is incorrect. Where you see
    sqrt(z^2-1) should actually be sqrt(z-1)*sqrt(z+1), and these are distinct
    for complex z!
    See also Principi et al, 10.1103/PhysRevB.80.075418 for plots of these
    functions calculated properly.

    Analyticity & correctness
    =========================
    This function is correct & analytic for Re(omega) > 0, Im(omega >= 0),
    and positive real k_x. This analyticity in upper half plane means that
    the relaxation time approximation can be used.

    In the plasmon sector this is also analytic for various complex k_x,
    which allows usage for finding decay lengths.
    """
    k_F = np.sqrt(np.pi*abs(n_s))
    def G(z):
        z = z+0.j
        sz2m1 = np.sqrt(z-1.)*np.sqrt(z+1.)
        return z*sz2m1 - np.log(z + sz2m1)

    Dplus  = (omega/v_F + 2.*k_F)/k_x
    Dminus = (omega/v_F - 2.*k_F)/k_x

    it1 = 8.*k_F/(v_F*k_x*k_x)
    it2 = (np.where(np.real(Dminus) < -1.,
                    G(-Dminus), # low w and low k
                    G(Dminus) + 1.j*np.pi  # high w or high k
                    )
           - G(Dplus)
          ) / np.sqrt(0.j + omega*omega - v_F*v_F*k_x*k_x)

    return -0.5j*conductance_quantum*omega*(it1+it2)

def DCsusceptibility_RPA0(k_x,n_s,v_F=1e6):
    """
    Graphene sheet susceptibility (screening) at 0 frequency, 0 temperature,
    0 scattering.
    See conductivity_graphene_RPA0 for parameters' definitions.

    (This function is used for static screening calculations, and for the
    relaxation time approximation at higher frequencies).
    """
    k_F = np.sqrt(np.pi*abs(n_s))
    x = 2*k_F/k_x
    return conductance_quantum/(v_F*k_x)*(
                2*x - np.where(x<1, x*np.sqrt(1-x*x) - np.arccos(x), 0.)
                )

def conductivity_RPA(omega,k_x,n_s,tau,v_F=1e6):
    """
    Graphene sheet conductivity in RPA model with 0 temperature but with
    added scattering, incorporated through the Mermin relaxation time
    approximation.

    omega
        angular frequency in rad/s
    k_x
        in-plane wavevector in rad/m
    n_s
        sheet carrier density in m^-2
    tau
        relaxation time in s

    Comes from the supplement of:
      "Graphene Plasmonics: A Platform for Strong Light-Matter Interaction"
    can be found at:
      http://authors.library.caltech.edu/25493/2/nl201771h_si_001.pdf
    """
    w = omega + 1.j/tau
    con0 = conductivity_RPA0(w, k_x, n_s,v_F)
    dcsusc0 = DCsusceptibility_RPA0(k_x, n_s,v_F)
    return 1./(1./con0 - 1./(tau*omega*w*dcsusc0))

def conductivity_RPA_finiteT(omega,k_x,E_D, tau, kT=4.0729e-21,v_F=1e6):
    """
    Calculate longitudinal, nonlocal conductivity of graphene at finite
    temperature according to RPA. This implementation uses Fermi surface
    smearing. Added scattering is incorporated through the Mermin relaxation
    time approximation by first calculating the pure conductivity and
    pure susceptibility at finite temperature and complex frequency.

    BEWARE: this method will produce strange results in the vicinity of
    singularities that depend on carrier density. In particular, the
    interband threshold at k=0 will appear as a series of `ave_num` small
    step thresholds. For k > 0 this will be a non-issue.

    omega : array_like
        angular frequency in rad/s
    k_x : array_like
        in-plane wavevector in rad/m
    E_D : array_like
        Dirac point energy relative to Fermi level, in J
        Note this is not proportional to square root of carrier density,
        due to temperature smearing -- see ``electron_density_from_band_offset``
        to map between these.
    tau : array_like
        relaxation time in s
    kT : array_like,optional
        boltzmann constant*temperature in J
        Default corresponds to 295 K.
    v_F : array_like,optional
        Fermi velocity to use, in m/s.
    """

    w = omega + 1.j/tau # evaluate cond at positive Im part (growing perturbation).

    def getcon0(E_F):
        n_0 = (np.abs(E_F)/(hbar*np.sqrt(np.pi)*v_F))**2
        return conductivity_RPA0(w,k_x,n_0,v_F=v_F)
    conw = smear_Maldague(getcon0,-E_D, kT)

    def getsusc0(E_F):
        n_0 = (np.abs(E_F)/(hbar*np.sqrt(np.pi)*v_F))**2
        return DCsusceptibility_RPA0(k_x,n_0,v_F=v_F)
    dcsuscw = smear_Maldague(getsusc0,-E_D, kT)

    return 1./(1./conw - 1./(tau*omega*w*dcsuscw))



def conductivity_transverse_RPA0(omega,k_x,n_s,v_F=1e6):
    """
    Graphene sheet conductivity in RPA model with 0 temperature, 0 scattering,
    in terms of:

    omega
        angular frequency in rad/s
    k_x
        in-plane wavevector in rad/m
    n_s
        sheet carrier density in m^-2

    This is for TRANSVERSE CHANNEL i.e. for k perpendicular to E.
    Comes from Principi et al, 10.1103/PhysRevB.80.075418

    Note that the original equation there is incorrect. Where you see
    sqrt(z^2-1) should actually be sqrt(z-1)*sqrt(z+1), and these are distinct
    for complex z! See paper for functions plotted properly.

    Analyticity & correctness
    =========================
    This function is correct & analytic for Re(omega) > 0, Im(omega >= 0),
    and positive real k_x. This analyticity in upper half plane means that
    the relaxation time approximation can be used.

    In the plasmon sector this is also analytic for various complex k_x,
    which allows usage for finding decay lengths.
    """
    k_F = np.sqrt(np.pi*abs(n_s))
    def G(z):
        z = z+0.j
        sz2m1 = np.sqrt(z-1.)*np.sqrt(z+1.)
        return z*sz2m1 + np.log(z + sz2m1)

    Dplus  = (omega/v_F + 2.*k_F)/k_x
    Dminus = (omega/v_F - 2.*k_F)/k_x

    it1 = -8.*k_F/(v_F*k_x*k_x)
    it2 = -(np.where(np.real(Dminus) < -1.,
                    G(-Dminus), # low w and low k
                    G(Dminus) - 1.j*np.pi  # high w or high k
                    )
           - G(Dplus)
          ) * np.sqrt(0.j + omega*omega - v_F*v_F*k_x*k_x) / omega**2

    return -0.5j*conductance_quantum*omega*(it1+it2)

def conductivity_transverse_RPA(omega,k_x,n_s,tau,v_F=1e6):
    """
    Graphene sheet conductivity in RPA model with 0 temperature but with
    added scattering, incorporated through the Mermin relaxation time
    approximation.

    omega
        angular frequency in rad/s
    k_x
        in-plane wavevector in rad/m
    n_s
        sheet carrier density in m^-2
    tau
        relaxation time in s

    This is for TRANSVERSE CHANNEL i.e. for k perpendicular to E.

    Meaning of relaxation and strangeness for high k vectors
    ========================================================
    This function includes relaxation time in a naive way, and it is
    not so clear from the literature what is a better way to proceed.
    Since transverse currents are not associated with charge density
    oscillations, Mermin's modification does not apply.

    Literature can be located that cites Mermin's paper and mentioning
    transverse electric fields:
    https://scholar.google.es/scholar?q=transverse&cites=8477684500107227345&scipsc=1

    Here with graphene, one confusing behaviour is that for small omega and
    for k > 2*k_F, the imaginary part of this conductivity becomes extremely
    large, scaling as k/omega. This would imply a sheet diamagnetic
    susceptibility that varies as k/omega^2.
    """
    w = omega + 1.j/tau
    return conductivity_transverse_RPA0(w, k_x, n_s,v_F)



##################
# MISC UTILITIES #
##################

if 'fdint' in globals():
    _fdi1 = np.vectorize(fdint.fd2h)
else:
    def _fdi1(z):
        """
        Evaluate Fermi-Dirac integral of order 1:
            int(x/(1 + exp(x - z)), x=0..infinty)

        z should be real

        Strategy: We only evaluate this integral up to some value just past z
            This is because:
                for  x-z >> +1 we have integrand ~= x*exp(z-x)
                which we can integrate exactly.

        TODO : this can be optimized that the lower limit of integration is
        closer to z. In that case this function can be used even for very large z.
        """

        dx = 0.025 # resolution for variable of integration
        gobeyond = 10.  # how many units (kT's) to go away from z

        # find range of z
        zmax = np.max(z)
        x = np.arange(0., zmax+gobeyond,dx)
        if x.size == 0: x = np.array([0.]) # make sure we have one element...
        xb = x.view()
        xb.shape = (x.size,)+(1,)*np.ndim(z)

        # Perform numerical integration
        integrand = xb/(1. + np.exp(xb - z))
        integral1 = dx*(np.sum(integrand,axis=0)
                        - 0.5*integrand[0]
                        - 0.5*integrand[-1])

        # Extrapolation integral
        xmax = x[-1]
        # integral of x*exp(z-x) = -exp(z-x)*(1+x) + const
        integral2 = np.exp(z-xmax)*(1+xmax)

        return integral1 + integral2

def electron_density_from_band_offset(E_D, kT=4.0729e-21,v_F=1e6):
    """
    Calculate electron density from Dirac point energy, including thermal
    smearing effects.

    E_D :
        Dirac point energy relative to Fermi level, in J
    kT : optional
        boltzmann constant*temperature in J
        Default corresponds to 295 K.
    """
    if kT == 0.: # special zero-temperature case
        return (-1./np.pi) * (E_D/(hbar*v_F))**2 * np.sign(E_D)

    f = -E_D/kT
    return (2./np.pi) * (kT/(hbar*v_F))**2 * (_fdi1(f) - _fdi1(-f))
