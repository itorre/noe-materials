"""
Material parameters for generic two-dimensional electron gas with parabolic
electron energy-momentum law.

* AC conductivity functions from various models
    - Simple Drude model
    - Nonlocal RPA (finite and zero temperature)
    - Nonlocal RPA + relaxation time (finite and zero temperature)
* Utilities
    - Band offset-electron density relationship incl. thermal smearing

Please carefully examine what functions take and return. Some use per-valley
and per-spin carrier density. Others take degeneracy factor and deal with the
TOTAL carrier density.


Conventions:
    Always use SI units!
        m^-2 for carrier density
        J for energies
        Hz for frequencies
        rad/s for angular frequencies
        etc.

    Imaginary part of epsilon should never be negative (gain).
    Real part of conductivity should never be negative (gain).

    When you add a material's parameters, you must provide citations to the
    relevant literature.
    Name the function specific, for example if you find the permittivity of
    SiO2 in a paper of Johnson et al., don't just call it SiO2.permittivity,
    but rather SiO2.permittivity_Johnson. Then if you think this is the
    "best" permittivity function to use, provide an alias.
"""

from .common import *




###############
# DRUDE MODEL #
###############

def conductivity_Drude(omega, n_s, tau, m=mass_electron):
    """
    Sheet conductivity (DRUDE MODEL) in terms of

    omega
        angular frequency in rad/s.
    n_s
        sheet carrier density in m^-2.
    tau
        transport scattering time in s.
    m
        effective mass in kilograms.
    """
    return (n_s * (elementary_charge**2/m)
            ) / (1./tau - 1.j*omega)


################
# NONLOCAL RPA #
################

# Check positivity of real part of square root
assert(np.sqrt(-1-0.1j).real >= 0)
assert(np.sqrt(-1+0.1j).real >= 0)
# Make sure log has imaginary part in [-pi,pi] interval
assert(abs(np.log(-1-0.001j).imag) <= np.pi)
assert(abs(np.log(-1+0.001j).imag) <= np.pi)

def conductivity_RPA0(omega,k_x,k_F,m=mass_electron):
    """
    Sheet conductivity PER VALLEY PER SPIN in RPA model with 0 temperature,
    0 scattering, in terms of:

    omega
        angular frequency in rad/s.
    k_x
        in-plane wavevector in rad/m.
    k_F
        Fermi wavevector in rad/m.
    m
        effective mass in kilograms; Fermi velocity = hbar k_F / m.

    Comes from Stern PRL 1967
      "POLARIZABILITY OF A TWO-DIMENSIONAL ELECTRON GAS"


    Note that the original equation is piecewise and for only real freq,
    wavevector. Here the formula has been simplified and analytically continued
    for complex frequency.

    Analyticity & correctness
    =========================
    This function is correct & analytic for Re(omega) > 0, Im(omega >= 0),
    and positive real k_x. This analyticity in upper half plane means that
    the relaxation time approximation can be used.

    For complex k, I have tried to choose branch cuts that make sense.
    At least inside the plasmonic sector (small k, large omega), you will
    not encounter these cuts.
    """
    n_s = (0.25/np.pi) * k_F**2
    v_F = k_F*hbar/m

    z = (0.5/k_F)*k_x**2  # For branch option 1
    u = omega/(v_F)
#    z = (0.5/k_F)*k_x  # For branch option 2
#    u = omega/(v_F*k_x)

    prefac = -1.j*omega*n_s * (elementary_charge**2/m) / (z*k_x**2 * v_F**2)

    # In branch option 1, we have lots of imaginary units here to rotate
    # branch cuts to the preferable locations in complex k space

    return prefac * (2*z # For branch option 1
                     + 1.j* np.sqrt(-1.j*(u-z-k_x))*np.sqrt(-1.j*(u-z+k_x))
                     - 1.j* np.sqrt(-1.j*(u+z-k_x))*np.sqrt(-1.j*(u+z+k_x))
                     )
#    return prefac * (2*z # For branch option 1
#                     + np.sqrt((u-z-k_x))*np.sqrt((u-z+k_x))
#                     - np.sqrt((u+z-k_x))*np.sqrt((u+z+k_x))
#                     )
#    return prefac * (2*z # For branch option 2
#                     + np.sqrt(u-z-1 + 0.j)*np.sqrt(u-z+1 + 0.j)
#                     - np.sqrt(u+z-1 + 0.j)*np.sqrt(u+z+1 + 0.j) )

def DCsusceptibility_RPA0(k_x,k_F,m=mass_electron):
    """
    DC susceptibility PER VALLEY PER SPIN in RPA model with 0 temperature,
    0 scattering, in terms of:

    k_x
        in-plane wavevector in rad/m.
    k_F
        Fermi wavevector in rad/m.
    m
        effective mass in kilograms; Fermi velocity = hbar k_F / m.

    Comes from Stern PRL 1967
      "POLARIZABILITY OF A TWO-DIMENSIONAL ELECTRON GAS"

    Analyticity & correctness
    =========================
    This function just varies as 1/k^2 for Re(k) < 2*k_F, and is guaranteed
    complex analytic in this region. For Re(k) = 2*k_F there are some branch
    cuts along pos and neg k.
    """
    n_s = (0.25/np.pi) * k_F**2
    v_F = k_F*hbar/m

    z = (0.5/k_F)*k_x
    prefac = n_s * (elementary_charge**2/m) / (k_x**2 * v_F**2)
    return prefac * (2 - np.where(np.abs(np.real(k_x)) > 2*k_F, np.sqrt(1 - z**-2 ), 0))


def conductivity_RPA(omega,k_x,n_s,tau,m=mass_electron,g=2):
    """
    Sheet conductivity in RPA model with 0 temperature but with
    added scattering, incorporated through the Mermin relaxation time
    approximation.

    omega
        angular frequency in rad/s.
    k_x
        in-plane wavevector in rad/m.
    n_s
        sheet carrier density (TOTAL) in m^-2.
    tau
        relaxation time in s.
    m
        effective mass in kilograms.
    g
        electron degeneracy factor (# valleys x # spins).
    """
    w = omega + 1.j/tau
    k_F = sqrt(4*pi*abs(n_s)/g)
    con0 = conductivity_RPA0(w, k_x, k_F,m=mass_electron)
    dcsusc0 = DCsusceptibility_RPA0(k_x, k_F,m=mass_electron)
    return g/(1./con0 - 1./(tau*omega*w*dcsusc0))


def conductivity_RPA_finiteT(omega,k_x,E_edge,tau,kT=4.0729e-21,m=mass_electron,g=2):
    """
    Sheet conductivity in RPA model with finite temperature and with
    added scattering, incorporated through the Mermin relaxation time
    approximation. Finite temperature is achieved through smear_Maldague().

    omega
        angular frequency in rad/s.
    k_x
        in-plane wavevector in rad/m.
    E_edge
        energy of band edge relative to Fermi level, i.e., (E_edge - E_F) in J.
    tau
        relaxation time in s.
    m
        effective mass in kilograms.
    g
        electron degeneracy factor (# valleys x # spins).
    """
    w = omega + 1.j/tau

    # ensure positive band for simplicity
    E_edge = E_edge * np.sign(m)
    m = np.abs(m)

    def getcon0(E): #argument is (E_F - E_edge)
        k_F = np.sqrt(2*m*E)/hbar
        return np.where(E>0,conductivity_RPA0(w,k_x,k_F,m=m),0)
    conw = smear_Maldague(getcon0,-E_edge, kT, E_F_min=0.)

    def getsusc0(E): #argument is (E_F - E_edge)
        k_F = np.sqrt(2*m*E)/hbar
        return np.where(E>0,DCsusceptibility_RPA0(k_x,k_F,m=m),0)
    dcsuscw = smear_Maldague(getsusc0,-E_edge, kT, E_F_min=0.)

    return g/(1./conw - 1./(tau*omega*w*dcsuscw))


def electron_density_from_band_offset(E_edge, kT=4.0729e-21,m=mass_electron,g=2):
    """
    Calculate electron density from band energy, including thermal
    smearing effects. This is total electron density, i.e., including degeneracy
    factor.

    E_edge
        Energy of band edge relative to Fermi level, i.e., (E_edge - E_F).
    kT : optional
        boltzmann constant*temperature in J.
        Default corresponds to 295 K.
    m
        Effective mass in kilograms.
        Note: if m>0 then larger carrier densities are for NEGATIVE E_edge.
        Likewise if m<0 then larger absolute densities are for POSITIVE E_edge.
        Also note for m<0 the result is NEGATIVE. (this is electron density,
        not carrier density)
    g
        electron degeneracy factor (# valleys x # spins)
    """

    DOS = m*g/(2*np.pi*hbar**2)
    # The Fermi dirac integral of order 0 is log(1+exp(z))
#    return DOS * kT * np.log(1 + np.exp(-np.sign(m) * E_edge / kT))  # BAD
    # We cannot use log(1+exp) due to overflow problems. But conveniently
    # numpy provides the logaddexp function! :-)
    return DOS * kT * np.logaddexp(0, -np.sign(m) * E_edge / kT) # GOOD
