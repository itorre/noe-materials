"""
Material parameters for silicon nitride
"""

from .common import *

cm_1 = 2*np.pi*lightspeed*1e2   # angular frequency from cm^-1

def permittivity_Kischkat(omega):
    """
    Permittivity of sputtered SiNx. Valid for about 18-195 THz (700-6500 cm^-1)

    From Kischkat et al, doi:10.1364/AO.51.006789 .
    We have chosen the "SN40-300" dataset as it it is claimed to be the properly
    stoichiometric result.
    """

#    Note - Paper is confusing!!!
#    \sigma is defined differently between table and equation. In the
#    table it is FWHM but in the equation it is standard deviation.
    fac = 1/(2*np.sqrt(2*np.log(2)))

    # SN40-300
    eps = 3.55 + 0.j*omega # baseline
    eps += ( 902*cm_1)**2 * lor_brendel(omega,  826*cm_1,  12  *cm_1, 171*cm_1*fac) # osc 1
    eps += ( 664*cm_1)**2 * lor_brendel(omega,  925*cm_1, 1e-50*cm_1, 163*cm_1*fac) # osc 2
    eps += ( 379*cm_1)**2 * lor_brendel(omega, 1063*cm_1,  58  *cm_1, 145*cm_1*fac) # osc 3
    eps += ( 172*cm_1)**2 * lor_brendel(omega, 1185*cm_1,  44  *cm_1, 104*cm_1*fac) # osc 4
    eps += ( 235*cm_1)**2 * lor_brendel(omega, 2577*cm_1, 1e-50*cm_1, 588*cm_1*fac) # osc 5

    # SN18-300 -- strangely this does not exactly fit the figure
#    eps = 3.06 + 0.j*omega # baseline
#    eps += (1247*cm_1)**2 * lor_brendel(omega,  826*cm_1,  20  *cm_1, 294*cm_1*fac) # osc 1
#    eps += ( 433*cm_1)**2 * lor_brendel(omega,  925*cm_1, 201  *cm_1, 440*cm_1*fac) # osc 2
#    eps += ( 100*cm_1)**2 * lor_brendel(omega, 1063*cm_1,   2  *cm_1, 781*cm_1*fac) # osc 3
#    eps += ( 191*cm_1)**2 * lor_brendel(omega, 1185*cm_1, 1e-50*cm_1, 355*cm_1*fac) # osc 4
#    eps += (37337./22128.)**2 * lor(omega, 22128*cm_1, 0*cm_1) # osc 5 : vis

    return eps

permittivity = permittivity_Kischkat
