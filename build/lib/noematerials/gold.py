"""
Material parameters for gold
"""

from .common import *

def permittivity_JC_Drude(omega):
    """
    Permittivity of gold, valid only in infrared.
    
    This is based on Johnson & Christy's "Optical Constants of the Noble Metals"
    http://dx.doi.org/10.1103/PhysRevB.6.4370
    in particular keeping in mind Figure 5 and 6 and the associated
    discussion and the table.
    
    - Figure 5 tells us the infinite-frequency permittivity as well as the
      drude weight. They did not explicitly name the infinite frequency
      permittivity but I have matched it based on the 1.02 eV tabulated
      n,k value with (n+1j*k)**2 =  (-66.2+5.7j)
    - Figure 6 should extrapolate to a zero intercept, and the slope tells
      us the relaxation time.

    This is valid up to hbar*omega ~= 1.5 eV, or around omega ~= 2*pi*400 THz.
    For higher frequencies, especially above 2 eV, additional loss channels
    appear from, e.g., interband transitions.
    See the above paper and also http://dx.doi.org/10.1103/PhysRev.138.A494 .
    """
    cond = conductivity_Drude(omega,
                              5.9e28, # number density - one electron per atom
                              0.99* mass_electron, # mass - from JC
                              1/9.3e-15) # scattering rate - from JC
    return 12.4 + (1.j / (omega * electric_constant)) * cond


permittivity = permittivity_JC_Drude
