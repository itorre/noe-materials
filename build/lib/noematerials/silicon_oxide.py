"""
Material parameters for silicon oxide
"""

from .common import *

cm_1 = 2*np.pi*lightspeed*1e2   # angular frequency from cm^-1
_SiO2_perm_list = [
    {'omega': 447*cm_1, 'gamma': 49*cm_1, 'sigma':.923},
    {'omega': 811*cm_1, 'gamma': 69*cm_1, 'sigma':.082},
    {'omega':1064*cm_1, 'gamma': 75*cm_1, 'sigma':.663},
    {'omega':1165*cm_1, 'gamma': 80*cm_1, 'sigma':.058},
    {'omega':1228*cm_1, 'gamma': 65*cm_1, 'sigma':.017},
    ]
def permittivity_Gunde(omega):
    """
    Permittivity of densified CVD-grown SiO2, and thermal SiO2.

    From Gunde, M. K.: "Vibrational modes in amorphous silicon dioxide"
        Physica B: Physics of Condensed Matter, Volume 292, Issue 3-4, p. 286-295.
    via http://fzu.cz/~dominecf/misc/meep/scripts/meep_materials.py
    """
    eps = 0.j*omega # construct complex
    eps += 2.4 # baseline
    for d in _SiO2_perm_list:
        eps += d['sigma']*lor(omega,d['omega'],d['gamma'])
    return eps


def permittivity_Palik(omega):
    """
    Permittivity of SiO2 for about 25-45 THz (~850 cm^-1 to ~1500 cm^-1).
    This includes only one phonon mode with wLO=1243.5*cm_1 and wTO=1065.5*cm_1 .
    From E. D. Palik, Handbook of Optical Costants of Solids (Elsevier, New York, 1997).
    """

    einf=1.843
    wLO=1243.5*cm_1
    wTO=1065.5*cm_1
    G=61.6*cm_1
    eps=einf*(1+(wLO**2-wTO**2)/(wTO**2-omega*(omega+1j*G)));

    return eps

def permittivity_Kischkat(omega):
    """
    Permittivity of sputtered SiO2. Valid for about 18-195 THz (700-6500 cm^-1)

    From Kischkat et al, doi:10.1364/AO.51.006789 .
    We have chosen the "SO18-200" data as it seems middle-of-the-line.
    """
    eps = 2.08 + 0.j*omega # baseline

#    Note - Paper is confusing!!!
#    \sigma is defined differently between table and equation. In the
#    table it is FWHM but in the equation it is standard deviation.
#    The following factor corrects this: we get visually the same as figure.
    fac = 1/(2*np.sqrt(2*np.log(2)))
    eps += (589*cm_1)**2 * lor_brendel(omega, 1046*cm_1,  7.20*cm_1,  77*cm_1*fac) # osc 1
    eps += (287*cm_1)**2 * lor_brendel(omega, 1167*cm_1,  1.20*cm_1, 123*cm_1*fac) # osc 2
    eps += (436*cm_1)**2 * lor_brendel(omega, 1058*cm_1,  2.24*cm_1,  67*cm_1*fac) # osc 3
    eps += (401*cm_1)**2 * lor_brendel(omega,  798*cm_1, 54.16*cm_1, 147*cm_1*fac) # osc 4
    return eps

permittivity = permittivity_Gunde
