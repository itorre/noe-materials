"""
Material parameters for titanium oxide
"""

from .common import *

cm_1 = 2*np.pi*lightspeed*1e2   # angular frequency from cm^-1

def permittivity_Kischkat(omega):
    """
    Permittivity of sputtered TiO2. Valid for about 18-195 THz (700-6500 cm^-1)

    From Kischkat et al, doi:10.1364/AO.51.006789 .
    We have chosen the "TO20-200" dataset as it looks typical.

    DO NOT USE BELOW 18 THz (700 cm-1) : SERIOUSLY UNPHYSICAL!
    Actual TiO2 has a resonance at 5.5 THz (183 cm^-1), not captured here.
    """

#    Note - Paper is confusing!!!
#    \sigma is defined differently between table and equation. In the
#    table it is FWHM but in the equation it is standard deviation.
    fac = 1/(2*np.sqrt(2*np.log(2)))

    eps = 5.83 + 0.j*omega # baseline
    eps += (1878*cm_1)**2 * lor_brendel(omega,   46*cm_1,  18  *cm_1, 732*cm_1*fac) # osc 1
    eps += (4590*cm_1)**2 * lor_brendel(omega,10815*cm_1, 160  *cm_1, 146*cm_1*fac) # osc 2

    return eps

permittivity = permittivity_Kischkat
