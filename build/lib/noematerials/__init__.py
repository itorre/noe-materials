"""
Materials parameters from literature.

Please follow these conventions when adding new materials:
    Always use SI units!
        m^-2 for carrier density
        J for energies
        Hz for frequencies
        rad/s for angular frequencies
        etc.

    For fourier-space quantities (frequency or wavevector dependent):
    Generally quantities shall be assumed to oscillate as:
        exp(i k x - i w t)
    Therefore,
      -Imaginary part of complex permittivity should never be negative (gain).
      -Real part of conductivity should never be negative (gain).

    When you add a material's parameters, you must provide citations to the
    relevant literature.
    Name the function specific, for example if you find the permittivity of
    SiO2 in a paper of Johnson et al., don't just call it SiO2.permittivity,
    but rather SiO2.permittivity_Johnson. Then if you think this is the
    "best" permittivity function to use, provide an alias.

    If there is a material that others may be relying on, please be careful
    with changes. Only modify parameters on a well-used material model if
    you think there was an error; afterwards, please leave a line on the end
    of the docstring to indicate that a change was made, and why.
"""

from . import graphene
Gr = graphene

from . import hexagonal_boron_nitride
hBN = hexagonal_boron_nitride

from . import hexagonal_molybdenum_disulfide
hMoS2 = hexagonal_molybdenum_disulfide

from . import silicon_oxide
SiO2 = silicon_oxide

from . import silicon
Si = silicon

from . import gold
Au = gold

from . import black_phosphorus
BP = black_phosphorus
