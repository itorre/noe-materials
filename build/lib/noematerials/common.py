"""
Common things for materials

Always follow conventions
Conventions:
    Always use SI units!
        m^-2 for carrier density
        J for energies
        Hz for frequencies
        rad/s for angular frequencies
        etc.

    Imaginary part of epsilon should never be negative (gain).
    Real part of conductivity should never be negative (gain).

    When you add a material's parameters, you must provide citations to the
    relevant literature.
    Name the function specific, for example if you find the permittivity of
    SiO2 in a paper of Johnson et al., don't just call it SiO2.permittivity,
    but rather SiO2.permittivity_Johnson. Then if you think this is the
    "best" permittivity function to use, provide an alias.


"""

import numpy as np
import scipy.special

# Physical constants from NIST CODATA 2014, http://physics.nist.gov/cuu/Constants/index.html
hbar = 1.054571800e-34 # J.s
boltzmann = 1.38064852e-23 # J/K
conductance_quantum = 0.5*7.7480917310e-5 # e^2/h in siemens
lightspeed = 299792458. #m/s
magnetic_constant = 4*np.pi*1e-7 # V.s/(A.m)
electric_constant = 1./(lightspeed**2*magnetic_constant)
epsmu = 1./(lightspeed*lightspeed)
eV = 1.6021766208e-19 # J
elementary_charge = 1.6021766208e-19 # C
mass_electron = 9.10938356e-31 # kg


####
# Material permittivity models
####

def lor(omega, omega_r, gamma):
    """
    Lorentz shape - damped oscillator. (see source for definition)
    Used for vibrational modes in permittivities.
    """
    return (omega_r**2)/(omega_r**2 - 1.j*omega*gamma - omega**2)

def lor_brendel(omega, omega_r, gamma, omega_r_sdev):
    r"""
    Much like ``lor()`` but where `omega_r` in the denominator is smeared by a
    Gaussian distribution. The added parameter `omega_r_sdev` sets the standard
    deviation of the smearing. This function does not use numerical integration
    but rather calls ``scipy.special.wofz`` .

    .. math::
        f(\omega;\omega_r,\gamma,\sigma) = \int_{-\infty}^{\infty} dx \, \frac{e^{-(x - \omega_r)^2/2\sigma^2}}{\sigma\sqrt{2\pi}} \frac{1}{x^2 - i\omega\gamma - \omega^2}

    Important: this lorentzian does not have a numerator, so the result has
    inverse-frequency-squared units. You can multiply by ``omega_r**2`` to get
    approximately a unity response at low freq, i.e., roughly the same as
    ``lor()``. However...

    Beware!! This does not work for near-zero frequencies. Not only is
    it numerically unstable, but also (especially for large sdev) there is
    a quasi-Drude pole at zero frequency. Effectively the smearing of sdev
    is a smearing of the restoring force / spring constant; some of the tail of
    this smearing has zero spring constant, hence effectively free carrier motion.

    Final warning: Don't pass in gamma=0, always use at least a tiny positive value.
    """
    # To get this result we use Abrahamowitz & Stegun's equation 7.1.4 and 7.1.11,
    # applied to the partial-fraction decomposition of the Lorentzian.
    ir2s = np.sqrt(0.5)/omega_r_sdev
    a = 1.j*np.sqrt(-omega*(omega + 1.j*gamma)) # ensure a has positive imaginary part
    t1 = (a - omega_r)*ir2s
    t2 = (a + omega_r)*ir2s
    return ir2s*1.j*np.sqrt(np.pi)*0.5/a * ( scipy.special.wofz(t1) + scipy.special.wofz(t2))

def lor_brendel2(omega, omega_r, gamma, omega_r_sdev):
    r"""
    Much like ``lor()`` but where `omega_r` in the is smeared by a
    Gaussian distribution. The added parameter `omega_r_sdev` sets the standard
    deviation of the smearing. This function does not use numerical integration
    but rather calls ``scipy.special.wofz`` . Unlike ``lor_brendel``, this
    smears both the numerator and denominator, and so it behaves properly at
    zero frequency.

    .. math::
        f(\omega;\omega_r,\gamma,\sigma) = \int_{-\infty}^{\infty} dx \, \frac{e^{-(x - \omega_r)^2/2\sigma^2}}{\sigma\sqrt{2\pi}} \frac{x^2}{x^2 - i\omega\gamma - \omega^2}

    Warning: Don't pass in gamma=0, always use at least a tiny positive value.
    """
    # To get this result we use Abrahamowitz & Stegun's equation 7.1.4 and 7.1.11,
    # applied to the partial-fraction decomposition of the Lorentzian.
    ir2s = np.sqrt(0.5)/omega_r_sdev
    a = 1.j*np.sqrt(-omega*(omega + 1.j*gamma)) # ensure a has positive imaginary part
    t1 = (a - omega_r)*ir2s
    t2 = (a + omega_r)*ir2s
    return 1 + a*ir2s*1.j*np.sqrt(np.pi)*0.5 * ( scipy.special.wofz(t1) + scipy.special.wofz(t2))

def smear_Maldague(func, mu, kT, ave_num=101, ave_spread=15., E_F_min=None):
    r"""
    Use Maldague's trick, e.g., as documented in arXiv:1512.06841v1 section II.C,
    to calculate a temperature-smeared Fermi surface quantity from its zero
    temperature form.

    .. math::
        \chi_T(\mu) = \int_{-\infty}^{\infty} \frac{d\delta}{4 \cosh^2(\delta/2)} \chi_0(\mu + kT\delta)

    func : function
        of the form func(E_F) where E_F is the internal chemical potential at
        zero temperature (i.e. "Fermi energy").
        Should return a quantity or array_like that can be averaged.
    mu : array_like
        Internal chemical potential (finite temperature).
    ave_num : int, optional
        How many averages to consider. The computation time will be this number
        times the time required to compute zero temperature conductivity.
    ave_spread : float, optional
        How wide of energy range to average over, scaled to temperature.
        A value of 10 means the averaging will be for mu-10*kT to mu+10*kT.
    E_F_min : float, optional
        Is there a minimum E_F below which the quantity will evaluate to zero?
        If yes, then provide it: if the normal range exceeds this bound then
        the range will be shifted to start just above E_F_min.

    Implementation
    ==============
    Current implementation will calculate this as a finite-difference sum by
    computing the function ave_num times, spreading from mu-ave_spread*kT
    to mu+ave_spread*kT. The default ave_spread = 15 means the weighting
    decreases to 1e-6 at the ends.

    For graphene don't provide E_F_min since the action happens around the
    Fermi surface. But do provide E_F_min for gapped systems, since
    for low densities the chemical potential can be well below the gap.
    """

    accum = 0.
    d_d = 2*ave_spread/(ave_num-1)
    if E_F_min is None:
        min_d = -ave_spread
    else:
        min_d = np.maximum(-ave_spread, (E_F_min-mu)/kT + 0.5*d_d)

    for i in range(ave_num):
        d = i*d_d + min_d
        E_F = mu + kT*d
        factor = 0.25*d_d * np.cosh(0.5*d)**-2
        accum = accum + factor*func(E_F)
    return accum

def conductivity_Drude(omega, n, m, sr):
    """
    Generic Drude model AC conductivity.

    Parameters
    ----------

    omega :
        angular frequency (rad/s)

    n :
        Free electron density (m^-3), relative to intrinsic case.
        This is equal to # of conduction band electrons minus # of valence
        band holes. `n` is positive for n-doped silicon and negative for
        p-doped silicon.

    m :
        Effective mass (in kg) of carriers.

    sr :
        Scattering rate (in s^-1) of carriers.
    """
    return n*(elementary_charge*elementary_charge/m) / (sr - 1.j*omega)


def conductivity_Drude_semiconductor(
                        omega,
                        n, n_i,
                        mcond_c, mcond_v,
                        mobility_c, mobility_v,
                        ):
    """
    Free-electron conductivity model for semiconductors considering both bands.
    This for frequencies below band gap i.e., in the infrared and below!

    Gives the conductivity contribution from two contributions in parallel:

    - Drude model conductance of conduction band.
    - Drude model conductance of valence band.

    In other words this does not include bound charge AC conductivity, and does
    not include interband conductivity!

    Parameters
    ----------

    omega :
        angular frequency (rad/s)

    n :
        Free electron density (m^-3), relative to intrinsic case.
        This is equal to # of conduction band electrons minus # of valence
        band holes. `n` is positive for n-doped silicon and negative for
        p-doped silicon.

    n_i :
        Intrinsic carrier density (m^-3) in conduction and valence bands.

    mcond_c, mcond_v :
        Conduction and valence band effective masses for conductivity,
        in units of true electron mass.
        Note: These are different from density of states effective masses!

    mobility_c, mobility_v :
        Conduction and valence band mobility in SI STANDARD UNITS (m^2/V/s).
    """

    n_c = 0.5*(n + np.sqrt(n*n + 4*n_i*n_i))
    n_v = n_c - n

    m_c = mcond_c * mass_electron
    m_v = mcond_v * mass_electron

    # calculate scattering times.
    sr_c = elementary_charge/(mobility_c*m_c)
    sr_v = elementary_charge/(mobility_v*m_v)

    # free charge conductivity
    return (  conductivity_Drude(omega, n_c, m_c, sr_c)
            + conductivity_Drude(omega, n_v, m_v, sr_v)
            )

