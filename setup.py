import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name = "noe-materials",
    version = "1.0",
    author = "NOE Group-ICFO, Iacopo Torre",
    author_email = "iacopo.torre@icfo.eu",
    description = "Optical properties of several materials",
    long_description = long_description,
    long_description_content_type = "text/markdown",
    url="https://gitlab.com/itorre/noe-materials",
    packages =setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python",
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
        "Operating System :: OS Independent",
    ],
    install_requires = ['numpy', 'scipy'],
    extras_require = {'FermiDiracIntegrals':  ['fdint']},
)